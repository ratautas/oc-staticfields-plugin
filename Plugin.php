<?php

namespace Ratauto\StaticFields;

use Event;
use Yaml;
use Cms\Classes\Theme;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'Static Fields Plugin',
            'description' => 'Add sane control of Static Page fields. Fields in ACTIVE_THEME/meta/fields/LAYOUT_FILE.yaml automatically appear in Page Edit UI',
            'author' => 'Algirdas Tamasauskas',
        ];
    }

    public function boot()
    {
        Event::listen('backend.form.extendFieldsBefore', function (\Backend\Widgets\Form $widget) {
            // Check if Edit window is actually Page interface
            if (
                !$widget->getController() instanceof \RainLab\Pages\Controllers\Index ||
                !$widget->model instanceof \RainLab\Pages\Classes\Page ||
                $widget->isNested
            ) {
                return;
            }

            if (!isset($widget->model->viewBag['layout'])) {
                return;
            }

            // Get string ID of lyout name.
            $layout = $widget->model->viewBag['layout'];

            // Check if corresponding .yaml file exists in ACTIVE_THEME/meta/fields
            if (!file_exists(themes_path(Theme::getActiveThemeCode().'/meta/fields/'.$layout.'.yaml'))) {
                return;
            }

            // Get fields from YAML
            $fields = Yaml::parseFile(themes_path(Theme::getActiveThemeCode().'/meta/fields/'.$layout.'.yaml'));

            // If YAML file has values at all:
            if (!empty($fields)) {
                
                // Add fields to the viewBag. With translation!
                foreach ($fields as $name => $field) {
                    $field['cssClass'] = isset($field['cssClass']) ? ' '.$field['cssClass'].' secondary-tab ' : 'secondary-tab ';
                    $widget->secondaryTabs['fields']['viewBag['.$name.']'] = $field;
                    $widget->model->translatable[] = 'viewBag['.$name.']';
                }
            }
        });
    }
}
